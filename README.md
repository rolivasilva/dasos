# Configurar Hyperledger Fabric para el dominio dasos.io

Configuración del `peer1` para la organización `walmart` del dominio `dasos.io`, en caso de que se este configurando otro peer (peer2, peer3 etc) se debe cambiar los archivos de configuración su respectivo equivalente. 

---
## Summary

- [Prerequisitos](#prerequisitos)
- [Instalación de los Componentes](#instalacion-de-los-componentes)
- [Inicializar fabric](#inicializar-fabric)
- [Comandos basicos para la Administración de dasos-netwok](#Comandos-basicos-para-la-administración-de-dasos-netwok)

---

### Prerequisitos

Para correr el  `Hyperledger Composer` y `Hyperledger Fabric`, se recomienda al menos 4Gb de memoria RAM y 20 GB Disco duro libre

Los siguientes versiones son la requeridas y previamente probadas para el funcionamiento del proyecto, de cambiar las mismas, se debe realizar todas las pruebas y estar pendiente en caso de posibles fallas:

- Operating Systems: Ubuntu Linux 16.04 64-bit.
- Docker Engine: Version 17.12.1 or higher
- Docker-Compose: Version 1.13 o
- Node: 8.10.0 or higher (Version 9 no esta soportada)
- npm: v5.7.1
- git: 2.9.x or higher
- Python: 2.7.12
- Composer: 0.16.6 (Version 0.19 no esta soportada)

Si la instalación la vamos a realizar desde cero, les recomiendo crear el usuario `ubuntu` y darle permiso para ejecutar el servidor con este usuario.

```bash
useradd -m -d /home/ubuntu ubuntu -s /bin/bash  && echo "ubuntu:ubuntu" | chpasswd && adduser ubuntu sudo
su - ubuntu
```

Antes de instalar los prerequisitos, vamos a descargar de `bitbucker` el proyecto de hyperledger para el dominio dasos.io, el cual lo ejecutaremos desde la raiz del usuario

```bash
cd ~ && git clone https://rolivasilva@bitbucket.org/rolivasilva/dasos.git peer1
cd peer1
git checkout -b peer1
git reset --hard origin/peer1
```

El siguiente paso lo ejecutaremos si el servidor esta recien creado o aun no se ha instalado ninguno de los requisitos
```bash
cd ~/peer1/config && ./prereqsUbuntu.sh
```

* Por favor, recuerde recargar las variables de entorno si realiza la instalación de los prerequisitos
---

### Instalar de los Componentes

La instalación de los componentes dependera de los servicios que se instalen, en este server instalaremos `composer-cli`.

```bash
npm install -g composer-cli
npm install -g composer-rest-server
npm install -g generator-hyperledger-composer
npm install -g composer-playground
```
---

### Inicializar fabric

- Inicializar los contenedores (peer1 y couchdb) de hyperledger, antes debemos editar el archivo `startFabric.sh` para cambiar el peer a su numero correspondiente y editar las ip de los peer  

```bash
pico ~/peer1/fabric-scripts/hlfv1/startFabric.sh

cat << EOF > .env
PEER=peer1.walmart.dasos.io
IP_PEER0=104.131.62.23
IP_PEER1=10.66.66.6
IP_PEER2=165.227.30.225
IP_CA=104.131.62.23
IP_ORDERER=104.131.62.23
EOF

# fetch the channel
docker exec -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@walmart.dasos.io/msp" peer1.walmart.dasos.io peer channel fetch config -o orderer.dasos.io:7050 -c composerchannel
# Join peer1.walmart.dasos.io to the channel.
docker exec -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@walmart.dasos.io/msp" peer1.walmart.dasos.io peer channel join -b composerchannel_config.block
```

ya estamos listos para inicializar el fabric con el peer configurado

```bash
cd ~/peer1 && ./startFabric.sh
```


### Comandos basicos para la Administración de dasos-netwok

Aca tenemos algunos de los comandos importantes para la administración de la network y sus componentes.


- Inicializar el servicio de Rest Server de hyperledger, el cual levanta el puerto 3000 del guest 

```bash
cd ~/peer1 && ./startRestServer.sh
```
Desde un explorador puedes visuailizar el rest server http://{DIRECION_IP}:3000

- Para importar la card tanto en el peer1 como en los otros peer utilizar este comando
```bash
cd ~/dasos
./startFabric.sh
./startDasos.sh
```

- Para chequear  que la business network se haya desplegado de forma exitosa, ejecutamos el siguiente comando desde cualquier peer

```bash
composer network ping --card admin@dasos-network

The connection to the network was successfully tested: dasos-network
        version: 0.16.6
        participant: org.hyperledger.composer.system.NetworkAdmin#admin

Command succeeded
```
