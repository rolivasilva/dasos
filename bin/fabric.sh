#!/bin/bash

## include
. ../lib/commons.sh
. ../lib/peers.sh
. ../lib/channel.sh

## Options
Action=$1
case $Action in
    container:start)
        start "$@"
        ;;
    container:stop)
        stop "$@"
        ;;
    peeradmin:create)
        create_peerAdmin "$@"
        ;;
    peeradmin:start)
        start_peerAdmin "$@"
        ;;
    channel:generate)
        generate_channel "$@"
        ;;
    channel:extend)
        extend_channel "$@"
        ;;

    *)
        echo "Sorry, I don't understand this action"
        echo ''
        echo "Usage: $0 {container:start|container:stop|peeradmin:create|channel:generate|channel:extend|peeradmin:start}" >&2
        echo 'help fabric command'
        echo ''
        echo 'fabric.sh <command>'
        echo ''
        echo 'Command:'
        echo '   fabric.sh container:start'
        echo '   fabric.sh container:stop'
        echo '   fabric.sh peeradmin:create'
        echo '   fabric.sh channel:generate'
        echo '   fabric.sh channel:extend'
        echo ''
        echo 'Options:'
        echo '   --help       || -h         Show help'
        exit 1
        ;;
esac


