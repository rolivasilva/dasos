#!/bin/bash

generate_channel(){   
    #params    

    #variables
    CRYPTO_CONFIG_FLIE="../composer/crypto-config.yaml"
    ORG=walmart
    DOMAIN=dasos.io

    #options
    SHORT=cph
    LONG=channelName:,peerNumbers:,help
    USERS=0
    generateNew='false'

    # process
    OPTIONS=$(getopt -o $SHORT -l $LONG -- "$@")
    if [ $? != 0 ] ; then create_usage start >&2 ; exit 1 ; fi
    eval set -- $OPTIONS
    while [ $# -gt 0 ]
    do
        case $1 in
            -c|--channelName) channelName=$2;shift;;
            -n|--peerNumbers) peerNumbers=$2;shift;;
            -h|--help) create_usage start;;
            (--) shift; break;;
            (-*) echo "$0: error - unrecognized option $1" 1>&2; create_usage_channel generate exit 1;;
            (*) break;;
        esac
        shift
    done

    # Validation

    #requirements

    # welcome message
    
    #process
    #crear el archivo crypto-config.ymal con la cantidad de peer a generar    
    createCryptoConfigFile
    generatePeer
    composerGenesis
    composerChannel

    
    exit 0 
}

extend_channel(){   
    #params
    
    #variables
    CRYPTO_CONFIG_FLIE="../composer/crypto-config.yaml"
    ORG=walmart
    DOMAIN=dasos.io

    #options
    SHORT=cph
    LONG=channelName:,peerNumbers:,help
    USERS=0
    generateNew='false'

    # process
    OPTIONS=$(getopt -o $SHORT -l $LONG -- "$@")
    if [ $? != 0 ] ; then create_usage start >&2 ; exit 1 ; fi
    eval set -- $OPTIONS
    while [ $# -gt 0 ]
    do
        case $1 in
            -c|--channelName) channelName=$2;shift;;
            -n|--peerNumbers) peerNumbers=$2;shift;;
            -h|--help) create_usage start;;
            (--) shift; break;;
            (-*) echo "$0: error - unrecognized option $1" 1>&2; create_usage_channel extend exit 1;;
            (*) break;;
        esac
        shift
    done

    # Validation

    #requirements

    # welcome message
    
    #process
    #crear el archivo crypto-config.ymal con la cantidad de peer a generar    
    createCryptoConfigFile
    extendPeer
    exit 0 
}

generatePeer(){
    cd ../composer
    cryptogen generate --config=./crypto-config.yaml
    cd -
}

extendPeer(){
    cd ../composer
    cryptogen extend --config=./crypto-config.yaml
    cd -
}

composerGenesis(){
    cd ../composer
    export FABRIC_CFG_PATH=$PWD
    configtxgen -profile ComposerOrdererGenesis -outputBlock ./composer-genesis.block
    cd -

}

composerChannel(){
    cd ../composer
    export FABRIC_CFG_PATH=$PWD
    configtxgen -profile ComposerChannel -outputCreateChannelTx ./composer-channel.tx -channelID $channelName
    cd -

}

createCryptoConfigFile(){
    echo ''
    echo 'Create the config file crypto-config.yaml...'
    echo ''
    cat << EOF > $CRYPTO_CONFIG_FLIE
OrdererOrgs:
  - Name: Orderer
    Domain: $DOMAIN
    Specs:
      - Hostname: orderer
PeerOrgs:
  - Name: Org1
    Domain: $ORG.$DOMAIN
    Template:
      Count: $peerNumbers
    Users:
      Count: $USERS
EOF
}


create_usage_channel(){
    echo 'help fabric command'
    echo ''
    echo 'fabric.sh <command>'
    echo ''
    echo 'Command:'
    echo '   fabric.sh container:start'
    echo '   fabric.sh container:stop'
    echo '   fabric.sh peeradmin:create'
    echo '   fabric.sh channel:generate'
    echo '   fabric.sh channel:extend'
    echo ''
    echo 'Options:'
    echo '   --help       || -h         Show help'
    Action=$1
    case $Action in
        generate)
            create_usage_generate
            ;;
        extend)
            create_usage_extend
            ;;
        *)
            echo ''
            exit 1
            ;;
    esac

    exit 0
}

create_usage_generate(){
    echo '   --channelName || -n  <name>   Name peer'
    echo '   --peerNumbers || -p  <number> amount peer'
}

create_usage_extend(){
    echo '   --channelName || -n  <name>   Name peer'
    echo '   --peerNumbers || -p  <number> amount peer'
}