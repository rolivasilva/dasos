#!/bin/bash


start(){   
    #params
    
    #variables
    ARCH_SERVER=`uname -m`
    VERSION="1.0.4"
    ARCH=$ARCH_SERVER"-"$VERSION
    peerName=''
    main='false'
    playground='false'
    rest='false'
    DIR_CONTAINER="$(cd ../containers && pwd)"
    #options

    SHORT=n:mhPR
    LONG=peerName:,main,help,playground,rest

    # process
    OPTIONS=$(getopt -o $SHORT -l $LONG -- "$@")
    if [ $? != 0 ] ; then create_usage start >&2 ; exit 1 ; fi
    eval set -- $OPTIONS
    while [ $# -gt 0 ]
    do
        case $1 in
            -m|--main) main='true';;
            -n|--peerName) peerName=$2;shift;;
            -P|--playground) playground='true';;
            -R|--rest) rest='true';;
            -h|--help) create_usage start;;
            (--) shift; break;;
            (-*) echo "$0: error - unrecognized option $1" 1>&2; create_usage start exit 1;;
            (*) break;;
        esac
        shift
    done

    # Validation
    validate_peerName
    validate_peerValid

    #requirements

    # welcome message
    echo 'Deploy peer: '$peerName
    echo 'Main Network:'$main
    echo 'Version Images Hyperledger: '$ARCH
    #process
    create_network
    echo "Deploy bind container..." && sleep 1
    start_bind
    if [ "$main" == 'true' ]; then
        echo "Deploy ca container..." && sleep 1
        start_ca
        echo "Deploy orderer container..." && sleep 1
        start_orderer
        echo "Deploy peer container..." && sleep 1
        start_peer
        if [ "$playground" == 'true' ]; then
            echo "Deploy playground container..." && sleep 1
            start_playground
        fi    
        if [ "$rest" == 'true' ]; then
            echo "Deploy rest server container..." && sleep 1
            start_rest
        fi            
    else
        echo "Deploy peer container..." && sleep 1
        start_peer
        if [ "$playground" == 'true' ]; then
            echo "Deploy playground container..." && sleep 1
            start_playground
        fi    
        if [ "$rest" == 'true' ]; then
            echo "Deploy rest server container..." && sleep 1
            start_rest
        fi            
    fi   
    join_channel
    exit 0 
}

stop(){

    #variables
    fabric='false'
    playground='false'
    rest='false'
    DIR_CONTAINER="$(cd ../containers && pwd)"
    #options

    SHORT=FPRh
    LONG=fabric,help,playground,rest

    # process
    OPTIONS=$(getopt -o $SHORT -l $LONG -- "$@")
    if [ $? != 0 ] ; then create_usage stop >&2 ; exit 1 ; fi
    eval set -- $OPTIONS
    while [ $# -gt 0 ]
    do
        case $1 in
            -F|--fabric) fabric='true';;
            -P|--playground) playground='true';;
            -R|--rest) rest='true';;
            -h|--help) create_usage stop;;
            (--) shift; break;;
            (-*) echo "$0: error - unrecognized option $1" 1>&2; create_usage stop exit 1;;
            (*) break;;
        esac
        shift
    done

    if [ "$fabric" == 'true' ]; then
        echo "Remove ca container..." && sleep 1
        stop_ca
        echo "Remove orderer container..." && sleep 1
        stop_orderer
        echo "Remove peer container..." && sleep 1
        stop_peer
    fi    
    if [ "$playground" == 'true' ]; then
        echo "Remove playground container..." && sleep 1
        stop_playground
    fi    
    if [ "$rest" == 'true' ]; then
        echo "Remove rest server container..." && sleep 1
        stop_rest
    fi            
    
}

stop_rest(){
    ARCH=$ARCH  docker-compose -f "${DIR_CONTAINER}"/rest/docker-compose.yml down
}

stop_playground(){
    ARCH=$ARCH  docker-compose -f "${DIR_CONTAINER}"/playground/docker-compose.yml down
}

stop_peer(){
    ARCH=$ARCH PEER=$PEER docker-compose -f "${DIR_CONTAINER}"/peer/docker-compose.yml down
}

stop_ca(){
    CA_KEY_FILE=$(ls ../composer/crypto-config/peerOrganizations/walmart.dasos.io/ca/ | grep _sk)
    ARCH=$ARCH CA_KEY_FILE=$CA_KEY_FILE docker-compose -f "${DIR_CONTAINER}"/ca/docker-compose.yml down
}

stop_orderer(){
    ARCH=$ARCH docker-compose -f "${DIR_CONTAINER}"/orderer/docker-compose.yml down
}

stop_bind(){
    ARCH=$ARCH PEER=$PEER docker-compose -f "${DIR_CONTAINER}"/bind/docker-compose.yml down
}

start_bind(){
    ARCH=$ARCH PEER=$PEER docker-compose -f "${DIR_CONTAINER}"/bind/docker-compose.yml up -d
    sleep 2
    IP_DNS=IP_DNS=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' bind9)
    echo $IP_DNS > ./.env    
}

start_rest(){
    ARCH=$ARCH  docker-compose -f "${DIR_CONTAINER}"/rest/docker-compose.yml up -d
}

start_playground(){
    ARCH=$ARCH  docker-compose -f "${DIR_CONTAINER}"/playground/docker-compose.yml up -d
}

start_peer(){
    ARCH=$ARCH PEER=$PEER docker-compose -f "${DIR_CONTAINER}"/peer/docker-compose.yml up -d
    docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' bind9
}

start_ca(){
    CA_KEY_FILE=$(ls ../composer/crypto-config/peerOrganizations/walmart.dasos.io/ca/ | grep _sk)
    ARCH=$ARCH CA_KEY_FILE=$CA_KEY_FILE docker-compose -f "${DIR_CONTAINER}"/ca/docker-compose.yml up -d
}

start_orderer(){
    ARCH=$ARCH docker-compose -f "${DIR_CONTAINER}"/orderer/docker-compose.yml up -d
}

create_network(){
    network=$(docker network ls| grep dasos.io)
    if [ "$network" == "" ]; then
        echo 'Create dasos.io Docker Network'
        docker network create dasos.io 
    else
        echo 'The dasos.io Network was created'
    fi 
}

validate_peerValid(){
PEER=$(ls ../composer/crypto-config/peerOrganizations/walmart.dasos.io/peers/ | grep $peerName$)
    if [ "$PEER" == "" ]; then
        echo 'Error: The peer '$peerName' does not have a valid certificate'
        exit 1
    fi
}

validate_peerName(){
    if [ "$peerName" == "" ]; then
        echo 'Error: The peer Name is required'
        create_usage start
        exit 1
    fi
}

join_channel(){
    docker inspect $peerName > /dev/null
    if [ $? -eq 0 ]; then
        echo 'Working channel' && sleep 10
        if [ "$main" == 'true' ]; then
            echo 'Creating the channel'
            docker exec $peerName peer channel create -o orderer.dasos.io:7050 -c composerchannel -f /etc/hyperledger/configtx/composer-channel.tx
            echo 'Join the:'$peerName' to the channel.'
            docker exec -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@walmart.dasos.io/msp" $peerName peer channel join -b composerchannel.block
        else
            echo 'fetching the channel'
            docker exec -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@walmart.dasos.io/msp" $peerName peer channel fetch config -o orderer.dasos.io:7050 -c composerchannel
            echo 'Join the:'$peerName' to the channel.'
            docker exec -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@walmart.dasos.io/msp" $peerName peer channel join -b composerchannel_config.block
        fi
    fi    
}


create_usage(){
    echo 'help hyperledger command'
    echo ''
    echo 'hyperledger.sh <command>'
    echo ''
    echo 'Command:'
    echo '   hyperledger.sh start'
    echo '   hyperledger.sh stop'
    echo ''
    echo 'Options:'
    echo '   --help       || -h         Show help'
    Action=$1
    case $Action in
        start)
            create_usage_start
            ;;
        stop)
            create_usage_stop
            ;;
        *)
            echo ''
            exit 1
            ;;
    esac

    exit 0
}

create_usage_start(){
    echo '   --peerName   || -n  <name> Name peer'
    echo '   --playground || -P         Deploy playgrund'
    echo '   --rest       || -R         Deploy rest server'
    echo '   --main       || -m         main server'
}

create_usage_stop(){
    echo '   --playground || -P         Destroy playgrund container'
    echo '   --rest       || -R         Destroy rest container'
    echo '   --fabric     || -F         Destroy fabric container'
}