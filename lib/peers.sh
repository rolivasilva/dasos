#!/bin/bash

create_peerAdmin(){   
    #params
    
    #variables
    PEERS_LS=($(ls ../composer/crypto-config/peerOrganizations/walmart.dasos.io/peers/))
    ORDERERS_LS=($(ls ../composer/crypto-config/ordererOrganizations/dasos.io/orderers/))
    PEERADMIN_CARD_FILE=/tmp/PeerAdmin@hlfv1.card
    PEER_FILE=/tmp/.peer
    ORDERERS_FILE=/tmp/.orderes
    CONNECTION_JSON=/tmp/.connection.json
    cp=${#PEERS_LS[@]}
    co=${#ORDERERS_LS[@]}
    DIR_COMPOSER=$(cd ../composer/crypto-config/&& pwd)
    DIR_MSP='peerOrganizations/walmart.dasos.io/users/Admin@walmart.dasos.io/msp'
    _sk=$(ls $DIR_COMPOSER/$DIR_MSP/keystore/)
    pem=$(ls $DIR_COMPOSER/$DIR_MSP/signcerts/)
    PRIVATE_KEY=$DIR_COMPOSER/$DIR_MSP'/keystore/'$_sk
    CERT=$DIR_COMPOSER/$DIR_MSP'/signcerts/'$pem
    COMPOSER_CLI=composer
    #options
    SHORT=h
    LONG=help

    # process
    OPTIONS=$(getopt -o $SHORT -l $LONG -- "$@")
    if [ $? != 0 ] ; then create_usage start >&2 ; exit 1 ; fi
    eval set -- $OPTIONS
    while [ $# -gt 0 ]
    do
        case $1 in
            -h|--help) create_usage start;;
            (--) shift; break;;
            (-*) echo "$0: error - unrecognized option $1" 1>&2; create_usage create exit 1;;
            (*) break;;
        esac
        shift
    done

    # Validation

    #requirements

    # welcome message
    echo 'Create peerAdmin Card'
    echo ''
    
    #process
    createConnection
    createPeerAdmin
    exit 0 
}

start_peerAdmin(){
    #params
    
    #variables
    COMPOSER_CLI=composer
    #options
    SHORT=h
    LONG=help

    # process
    OPTIONS=$(getopt -o $SHORT -l $LONG -- "$@")
    
    if [ $? != 0 ] ; then create_usage start >&2 ; exit 1 ; fi
    eval set -- $OPTIONS
    while [ $# -gt 0 ]
    do
        case $1 in
            -h|--help) create_usage start;;
            (--) shift; break;;
            (-*) echo "$0: error - unrecognized option $1" 1>&2; create_usage start exit 1;;
            (*) break;;
        esac
        shift
    done

    # Validation

    #requirements

    # welcome message
    echo ''
    echo ''
    
    #process
    createDasos 
    exit 0 
}
createDasos (){
    cd ../dasos-network/
    echo 'Deploying the business network'
    $COMPOSER_CLI runtime install --card PeerAdmin@hlfv1 --businessNetworkName dasos-network
    if [ $? -ne 1 ]; then
        echo 'Starting a business network card'
        $COMPOSER_CLI network start --card PeerAdmin@hlfv1 --networkAdmin admin --networkAdminEnrollSecret adminpw --archiveFile dasos-network@0.0.1.bna --file networkadmin.card
    else
      echo 'Error: No Deploy the bussines card'
      exit 1
    fi
    if [ $? -ne 1 ]; then
        echo 'Importing the business network card'
        $COMPOSER_CLI card import --file networkadmin.card
    else
        echo 'Error: No Start the bussines network'
        exit 1
    fi
}


createConnection(){

    i=0 
    touch PEER_FILE && touch ORDERERS_FILE 
    for PEER_CONF in "${PEERS_LS[@]}"
    do    
        i=$((i+1))
        if [ $i -lt $cp ]; then
            echo '{ "requestURL": "grpc://'$PEER_CONF':7051", "eventURL": "grpc://'$PEER_CONF':7053"},' >> PEER_FILE 
        else
            echo '{ "requestURL": "grpc://'$PEER_CONF':7051", "eventURL": "grpc://'$PEER_CONF':7053"}' >> PEER_FILE     
        fi
    done
    i=0
    for ORDERER_CONF in "${ORDERERS_LS[@]}"
    do    
        i=$((i+1))

        if [ $i -lt $co ]; then
            echo '{ "url" : "grpc://'$ORDERER_CONF':7050" },' >> ORDERERS_FILE 
        else
            echo '{ "url" : "grpc://'$ORDERER_CONF':7050" }' >> ORDERERS_FILE   
        fi
    
    done
    PEERS=$(cat PEER_FILE)
    ORDERERS=$(cat ORDERERS_FILE)
    rm -f PEER_FILE && rm -f ORDERERS_FILE
    cat << EOF > $CONNECTION_JSON
    {
        "name": "hlfv1",
        "type": "hlfv1",
        "orderers": [
            $ORDERERS
        ],
        "ca": { "url": "http://ca.walmart.dasos.io:7054", "name": "ca.walmart.dasos.io"},
        "peers": [
            $PEERS
        ],
        "channel": "composerchannel",
        "mspID": "Org1MSP",
        "timeout": 300
    }
EOF
}

createPeerAdmin(){
    echo 'Checking if the PeerAdmin card exist...'
    $COMPOSER_CLI card list -n PeerAdmin@hlfv1 >>/dev/null
    if [ $? -ne 1 ]; then
        echo 'Deleting PeerAdmin card'
        $COMPOSER_CLI card delete -n PeerAdmin@hlfv1    
    fi
    echo 'Creating the PeerAdmin card'
    $COMPOSER_CLI card create -p $CONNECTION_JSON -u PeerAdmin -c $CERT -k $PRIVATE_KEY -r PeerAdmin -r ChannelAdmin --file /tmp/PeerAdmin@hlfv1.card
    echo 'Importing the PeerAdmin card'
    $COMPOSER_CLI card import --file $PEERADMIN_CARD_FILE 
    rm -f $CONNECTION_JSON

    echo "Hyperledger Composer PeerAdmin card has been imported"
    $COMPOSER_CLI card list

}
create_usage(){
    echo 'help hyperledger command'
    echo ''
    echo 'hyperledger.sh <command>'
    echo ''
    echo 'Command:'
    echo '   hyperledger.sh start'
    echo '   hyperledger.sh stop'
    echo ''
    echo 'Options:'
    echo '   --help       || -h         Show help'
    Action=$1
    case $Action in
        start)
            create_usage_start
            ;;
        stop)
            create_usage_stop
            ;;
        *)
            echo ''
            exit 1
            ;;
    esac

    exit 0
}

create_usage_start(){
    echo '   --peerName   || -n  <name> Name peer'
    echo '   --playground || -P         Deploy playgrund'
    echo '   --rest       || -R         Deploy rest server'
    echo '   --main       || -m         main server'
}

create_usage_stop(){
    echo '   --playground || -P         Destroy playgrund container'
    echo '   --rest       || -R         Destroy rest container'
    echo '   --fabric     || -F         Destroy fabric container'
}